import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['exception1.py'],input_text="3")
    assert result == "取得する番号を入力して下さい：取得できませんでした\n"
    result = winrun.run(['exception1.py'],input_text="a")
    assert result == "取得する番号を入力して下さい：取得できませんでした\n"