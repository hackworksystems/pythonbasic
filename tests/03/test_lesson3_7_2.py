import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson3.7.2.py'],input_text="3\n4")
    # assert result.returncode == 0
    assert result == "縦の長さ（m）横の長さ（m）面積は 12.0 平方メートル\n"