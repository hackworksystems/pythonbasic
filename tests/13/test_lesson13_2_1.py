import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson13.2.1.py'])
    assert result == "タイトル Python入門 著者 リナックスアカデミー 値段 2000\n"