import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['private.py'])
    assert result == "アクセルを踏みます\nエンジンを始動します\nガソリンを燃焼してピストンを振動させます\n---\nエンジンを始動します\nガソリンを燃焼してピストンを振動させます\n---\n"