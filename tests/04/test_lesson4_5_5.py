import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson4.5.5.py'])
    assert result == "('月', '火', '水', '木', '金', '土', '日')\n"