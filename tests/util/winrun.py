import sys
import pathlib
import subprocess
import inspect

def run(args,input_text=""):
    # 実行されたtest_*.pyファイルのパスをCallStackから取得する
    testfile = inspect.stack()[1].filename
    # テストファイルのあるフォルダを取得する
    path = pathlib.Path(testfile).parent
    # テスト対象のファイルのあるフォルダへの相対パス
    chapter = pathlib.PurePath(path).parts[-1]
    path /= '../../practice/' + chapter
    #path /= '../../fix/' + chapter
    # 相対パスを絶対パスに変更する
    cwd = path.resolve()
    # print(cwd) 
    # 実行するコマンド
    cmd = ['python'] + args
    # コマンドを実行して結果オブジェクトを返す
    cp = subprocess.Popen(cmd,stdout=subprocess.PIPE,stdin=subprocess.PIPE,text=True,encoding='cp932',cwd=cwd)
    lines = input_text.splitlines()
    for line in lines:
        # print(line)
        cp.stdin.write(line+"\n")
    result = cp.communicate()
    # print(result)
    cp.stdin.close()
    return result[0]
        
