import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['truck1.py'])
    assert result == "フォワード 20 ['テーブル']\n"