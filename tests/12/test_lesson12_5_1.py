import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson12.5.1.py'])
    assert result == "Python入門 2000 リナックスアカデミー\n"