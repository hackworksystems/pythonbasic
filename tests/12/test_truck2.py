import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['truck2.py'])
    assert result == "カローラ 10\nフォワード 15.0\n"