import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson12.5.2.py'])
    assert result == "color red x 1 y 3\n"