import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['empty2.py'])
    assert result == "2 3 4\n2 4 6\n"