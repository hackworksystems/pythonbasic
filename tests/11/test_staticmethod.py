import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['staticmethod.py'])
    assert result == "発動機の動力で走る車を自動車といいます\n"