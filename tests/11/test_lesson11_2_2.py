import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson11.2.2.py'])
    assert result == "縦 10 横 2 面積 20\n縦 2 横 3 面積 6\n"