import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

# lesson11.2.1.pyのようにファイル名の途中に.を含むモジュールはimportが大変なのでテスト省略

def test_run():
    result = winrun.run(['lesson11.2.1.py'])
    assert result == ""