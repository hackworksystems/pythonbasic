import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['car5.py'])
    assert result == "(1): 0\n(2): 1\n(3): 2\n(4): 3\n(5): 0\n(6): 3\n(7): 3\n(8): 0\n"