import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson11.2.3.py'])
    assert result == "四角形の辺の個数は 4 です\n"