import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['output_json_file.py'])
    result = open(os.path.join(os.path.dirname(__file__),"../../practice/09/json_file.txt"), "r").read()
    assert result == '{"name": "\\u9234\\u6728\\u4e00\\u90ce", "age": 20, "tel": ["0312345678", "09012345678"]}'