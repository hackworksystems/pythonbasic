import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['input_file1.py'])
    assert result == "Pythonの世界へようこそ\nおはよう\nこんにちは\nこんばんは\n123\n\n"