import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson9.3.1.py'],input_text="12345\n67890\nあいうえお\nかきくけこ\nさしすせそ")
    result = open(os.path.join(os.path.dirname(__file__),"../../practice/09/lesson_text.txt"),"r").read()
    assert result == "12345\n67890\nあいうえお\nかきくけこ\nさしすせそ\n"