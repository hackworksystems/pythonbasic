import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson9.3.2.py'])
    assert result == "[0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]\n"