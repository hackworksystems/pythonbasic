import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'../../fix/15/'))

from shape import Rectangle
from shape import Triangle
from shape import Circle

def test_Rectangle():
    rect = Rectangle(5, 3)
    assert rect.area == 15
    assert Rectangle.point == 4

def test_Triangle():
    tri = Triangle(5, 3)
    assert tri.area == 7.5
    assert Triangle.point == 3

def test_Circle():
    cir = Circle(4)
    assert cir.area == 12.56
    assert Circle.point == 0