import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['use_shape2.py'],input_text="40\n50")
    assert result == "長方形の高さ：長方形の幅：入力エラー\n"

    result = winrun.run(['use_shape2.py'],input_text="4\n5\n5\n8\n2")
    assert result == "長方形の高さ：長方形の幅：三角形の底辺：三角形の高さ：円の直径：面積の合計は43.14平方cmです。\n"