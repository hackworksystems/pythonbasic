import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['use_shape.py'])
    assert result == "図形の面積は47.44平方cmです。\n角の数の合計は7個です。\n"