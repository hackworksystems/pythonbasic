import sys
import os
import pytest
sys.path.append(os.path.join(os.path.dirname(__file__),'../../fix/15/'))

from shape2 import Rectangle
from shape2 import Triangle
from shape2 import Circle

def test_Rectangle():
    rect = Rectangle(5, 3)
    assert rect.area == 15
    assert Rectangle.point == 4
    assert rect.width == 3
    assert rect.height == 5

def test_Rectangle_init_by_illegal_value():
    # exitするコードをテストする
    with pytest.raises(SystemExit) as exited:
        Rectangle(21,3)
    assert exited.value.code == None
    with pytest.raises(SystemExit) as exited:
        Rectangle(5,21)
    assert exited.value.code == None
    with pytest.raises(SystemExit) as exited:
        Rectangle(0,3)
    assert exited.value.code == None
    with pytest.raises(SystemExit) as exited:
        Rectangle(5,0)
    assert exited.value.code == None

def test_Triangle():
    tri = Triangle(5, 3)
    assert tri.area == 7.5
    assert Triangle.point == 3
    assert tri.base == 5
    assert tri.height == 3

def test_Triangle_init_by_illegal_value():
    # exitするコードをテストする
    with pytest.raises(SystemExit) as exited:
        Triangle(21,3)
    assert exited.value.code == None
    with pytest.raises(SystemExit) as exited:
        Triangle(5,21)
    assert exited.value.code == None
    with pytest.raises(SystemExit) as exited:
        Triangle(0,3)
    assert exited.value.code == None
    with pytest.raises(SystemExit) as exited:
        Triangle(5,0)
    assert exited.value.code == None

def test_Circle():
    cir = Circle(4)
    assert cir.area == 12.56
    assert Circle.point == 0
    assert cir.diameter == 4
    assert cir.radius == 2

def test_Circle_init_by_illegal_value():
    # exitするコードをテストする
    with pytest.raises(SystemExit) as exited:
        Circle(21)
    assert exited.value.code == None
    with pytest.raises(SystemExit) as exited:
        Circle(0)
    assert exited.value.code == None