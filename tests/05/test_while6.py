import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['while6.py'])
    assert result == "64\n90\n78\n82\n"