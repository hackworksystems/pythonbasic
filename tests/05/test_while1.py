import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['while1.py'])
    assert result == "0：Pythonの世界へようこそ\n1：Pythonの世界へようこそ\n2：Pythonの世界へようこそ\n3：Pythonの世界へようこそ\n4：Pythonの世界へようこそ\n"