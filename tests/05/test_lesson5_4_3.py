import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson5.4.3.py'])
    assert result == "1から10までの合計は55です\n"