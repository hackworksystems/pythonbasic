import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson5.4.5.py'])
    assert result == "7\n12\n37\n24\n2\n92\n51\n"