import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson5.4.6.py'])
    assert result == "*\n**\n***\n****\n*****\n"