import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['while4.py'],input_text="80\n50\n80\n90")
    assert result == "点数を入力して下さい：合格です\n点数を入力して下さい：不合格です\n追試点数を入力して下さい：追試合格です\n点数を入力して下さい：合格です\n"
