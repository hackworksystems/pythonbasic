import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson5.4.7.py'],input_text="34\n83\n56\n74\n69")
    assert result == "1番目の点数：2番目の点数：3番目の点数：4番目の点数：5番目の点数：最高得点は83です\n"