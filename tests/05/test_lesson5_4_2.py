import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson5.4.2.py'],input_text="10")
    assert result == "降水確率を入力して下さい：傘はいらないでしょう\n"
    result = winrun.run(['lesson5.4.2.py'],input_text="20")
    assert result == "降水確率を入力して下さい：傘はあった方がいいかもしれません\n"
    result = winrun.run(['lesson5.4.2.py'],input_text="70")
    assert result == "降水確率を入力して下さい：傘を必ず持って行きましょう\n"