import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson8.5.1.py'])
    assert result == "1 + 2 = 3\n1 - 2 = -1\n"