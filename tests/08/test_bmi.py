import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'../../practice/08/'))

import bmi

def test_bmi():
    assert bmi.get_bmi(1.70,50) == 17.301038062283737
    assert bmi.get_bmi(1.70,60) == 20.761245674740486
    assert bmi.get_bmi(1.70,70) == 24.221453287197235
    assert bmi.get_bmi(1.70,80) == 27.68166089965398
    assert bmi.get_bmi(1.70,90) == 31.14186851211073
    assert bmi.get_bmi(1.70,100) == 34.602076124567475
    assert bmi.get_bmi(1.70,110) == 38.062283737024224
    assert bmi.get_bmi(1.70,120) == 41.52249134948097

def test_display_bmi(capfd):
    bmi.display_bmi(18.4)
    out, err = capfd.readouterr()
    assert out == 'BMIは低体重\n'
    bmi.display_bmi(18.5)
    out, err = capfd.readouterr()
    assert out == '普通体重\n'
    bmi.display_bmi(24)
    out, err = capfd.readouterr()
    assert out == '普通体重\n'
    bmi.display_bmi(25)
    out, err = capfd.readouterr()
    assert out == '肥満度1\n'
    bmi.display_bmi(29)
    out, err = capfd.readouterr()
    assert out == '肥満度1\n'
    bmi.display_bmi(30)
    out, err = capfd.readouterr()
    assert out == '肥満度2\n'
    bmi.display_bmi(34)
    out, err = capfd.readouterr()
    assert out == '肥満度2\n'
    bmi.display_bmi(35)
    out, err = capfd.readouterr()
    assert out == '肥満度3\n'
    bmi.display_bmi(39)
    out, err = capfd.readouterr()
    assert out == '肥満度3\n'
    bmi.display_bmi(40)
    out, err = capfd.readouterr()
    assert out == '肥満度4\n'
  
