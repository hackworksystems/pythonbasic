import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson8.5.2.py'])
    assert result in ('大吉\n','中吉\n','小吉\n','凶\n')