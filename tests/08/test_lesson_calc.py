import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'../../practice/08/'))

import lesson_calc

def test_func_add():
    assert lesson_calc.func_add(1,2) == 3

def test_func_sub():
    assert lesson_calc.func_sub(1,2) == -1
