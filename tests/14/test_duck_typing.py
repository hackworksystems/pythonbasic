import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['duck_typing.py'])
    assert result == "トラック「ドゴゴゴゴゴゴ...」\n電気自動車「スー...」\n"