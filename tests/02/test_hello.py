import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['hello.py'])
    assert result == "Pythonの世界へようこそ\n"