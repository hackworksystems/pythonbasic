import re
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['function_object1.py'])
    assert re.fullmatch("3\n<function add_function at 0x[0-9A-F]*?>\n", result)