import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['anonymous_function3.py'])
    assert result == "{'english': 60, 'mathematics': 100, 'science': 50, 'society': 82}\n[('mathematics', 100), ('society', 82), ('english', 60), ('science', 50)]\n"