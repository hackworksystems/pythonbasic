import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__),'..'))

from util import winrun

def test_run():
    result = winrun.run(['lesson6.6.1.py'])
    assert result == "10÷2=5.0\n5÷2=2.5\n" 
