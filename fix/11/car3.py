class Car:
    '''車を表すクラス'''

    def __init__(self, car_model):
        '''コンストラクタ'''
        self.car_model = car_model  # 車種
        self.direction = '前'       # 進行方向
        self.speed = 0              # スピード

# オブジェクトの生成と同時に初期化
car_a = Car('カローラ') # A車
car_b = Car('アテンザ') # B車
# 各プロパティの表示
print(car_a.car_model, car_a.direction, car_a.speed)
print(car_b.car_model, car_b.direction, car_b.speed)
