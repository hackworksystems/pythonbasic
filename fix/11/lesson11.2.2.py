class Square:
    # コンストラクタ
    def __init__(self, heigth, width):
        self.height = heigth
        self.width = width

    # 面積を取得する
    def getAreaSize(self):
        return self.height * self.width

square1 = Square(10, 2)
square2 = Square(2, 3)
print('縦', square1.height, '横', square1.width, '面積', square1.getAreaSize())
print('縦', square2.height, '横', square2.width, '面積', square2.getAreaSize())
