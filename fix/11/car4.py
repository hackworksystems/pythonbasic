class Car:
    '''車を表すクラス'''

    def __init__(self, car_model = 'アテンザ', direction = '前', speed = 0):
        '''コンストラクタ'''
        self.car_model = car_model  # 車種
        self.direction = direction  # 進行方向
        self.speed = speed          # スピード

# オブジェクトの生成と同時に初期化
car_a = Car('カローラ')        # 第1引数のみ
car_b = Car()                 # 引数なし
car_c = Car('カローラ', '右', 10) # 第1,2,3引数設定
# 各プロパティの表示
print(car_a.car_model, car_a.direction, car_a.speed)
print(car_b.car_model, car_b.direction, car_b.speed)
print(car_c.car_model, car_c.direction, car_c.speed)
