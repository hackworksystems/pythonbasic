class Car:

    # 静的メソッド
    @staticmethod
    def introduce_car():
        print('発動機の動力で走る車を自動車といいます')

Car.introduce_car()
