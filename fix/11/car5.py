class Car:
    '''車を表すクラス'''

    # クラス変数
    num_cars = 0

    def __init__(self, car_model, direction = '前', speed = 0):
        '''コンストラクタ'''
        # インスタンス変数
        self.car_model = car_model  # 車種
        self.direction = direction  # 進行方向
        self.speed = speed          # スピード

        # Carクラスのオブジェクトさ生成されるたらnum_carsを+1する
        Car.num_cars += 1

# クラス変数の値を参照
print('(1):', Car.num_cars)
car_a = Car('カローラ')
print('(2):', Car.num_cars)
car_b = Car('アテンザ')
print('(3):', Car.num_cars)
car_c = Car('カローラ')
print('(4):', Car.num_cars)

# オブジェクト.クラス変数名のふるまい
car_a.num_cars = 0  # car_a.numを変更
print('(5):', car_a.num_cars) # car_a.numだけ値が異なる
print('(6):', car_b.num_cars)
print('(7):', car_c.num_cars)

car_d = Car('プリウス') # Car.car_numsをカウント
print('(8):', car_a.num_cars) # car_a.numだけ値は変わらない