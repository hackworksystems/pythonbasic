class Car:
    # クラス変数
    count = 0

    # クラスメソッド
    @classmethod
    def print_num_cars(cls):
        print('車の台数は' + str(cls.count) + 'です')

    def __init__(self, car_model):
        self.car_model = car_model

        # Carクラスのオブジェクトさ生成されるたらnum_carsを+1する
        Car.count += 1

car_a = Car('カローラ')
Car.print_num_cars() # クラス名.クラスメソッド名で呼び出し可
car_a.print_num_cars() # オブジェクト名.クラスメソッド名でも呼び出し可

car_b = Car('アテンザ')
Car.print_num_cars()
car_a.print_num_cars()
