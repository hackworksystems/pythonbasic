# 空のクラス
class User: pass # 改行後インデント＋passとしてもよい

# 空のクラスのオブジェクトを生成
user = User()
# インスタンス変数を設定
user.name = '鈴木一郎' 
user.age = 20

print(user.name, user.age)