class Car:
    '''車を表すクラス'''

    def set_car_model(self, car_model):
        '''車種を設定するメソッド'''
        self.car_model = car_model

    def get_car_model(self):
        '''車種を取得するメソッド'''
        return self.car_model    

car_a = Car()                  # オブジェクトを生成
car_a.set_car_model('カローラ') # 車種を設定
car_b = Car()
car_b.set_car_model('アテンザ')

print(car_a.car_model)         # プロパティを直接取得して表示
print(car_a.get_car_model())   # self.car_modelの値を取得して表示
print(car_b.car_model)
print(car_b.get_car_model())
