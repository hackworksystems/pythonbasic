class Car:
    '''車を表すクラス'''

    def handle(self, direction):
        '''進行方向を変更する'''
        self.direction = direction

    def accelerate(self, speed):
        '''加速する'''
        self.speed = speed

# A,B,C車のオブジェクトを生成
car_a = Car()
car_b = Car()
car_c = Car()
# ハンドルを左にきる
car_a.handle('左')
# アクセルを踏む
car_a.accelerate(10)
# car_aのプロパティを出力
print(car_a.direction, car_a.speed)
