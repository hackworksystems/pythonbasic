class Square:
    # 辺の個数
    SIDES = 4
 
    # コンストラクタ
    def __init__(self, heigth, width):
        self.height = heigth
        self.width = width

    # 面積を取得する
    def getAreaSize(self):
        return self.height * self.width

print('四角形の辺の個数は', Square.SIDES, 'です')
