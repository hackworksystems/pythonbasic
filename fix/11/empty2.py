# 空のクラス
class Calc: pass

# 空のクラスのオブジェクトを生成
calc = Calc()
# インスタンス変数に無名関数を代入
calc.func1 = lambda x: x + 1
calc.func2 = lambda x: 2 * x

print(calc.func1(1), calc.func1(2), calc.func1(3))
print(calc.func2(1), calc.func2(2), calc.func2(3))