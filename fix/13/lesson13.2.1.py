class Book:
    def __init__(self, title, author, price):
        self._title = title
        self._author = author
        self._price = price

    def get_book_info(self):
        print('タイトル', self._title, '著者', self._author, '値段', self._price)

book = Book('Python入門', 'リナックスアカデミー', 2000)
book.get_book_info()