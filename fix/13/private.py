class Car:
    # 外部から呼び出し可能なメソッド
    def __fire(self):
        print('ガソリンを燃焼してピストンを振動させます')

    # 外部から呼び出し非推奨なメソッド
    def _engine(self):
        print('エンジンを始動します')
        self.__fire()

    # 外部から呼び出し不可なメソッド
    def accelerate(self):
        print('アクセルを踏みます')
        self._engine()

car = Car()
car.accelerate()
print('---')
car._engine()
print('---')
car.__fire()
