class Car:
    def __init__(self, car_model):
        self._car_model = car_model # 車種
        self._gasoline = 0          # ガソリン

    # 車種のgetter
    @property
    def car_model(self):
        return self._car_model

    # ガソリンのgetter
    @property
    def gasoline(self):
        return self._gasoline

    # ガソリンのsetter
    @gasoline.setter
    def gasoline(self, gasoline):
        self._gasoline += gasoline
        # 150を上限にする
        if self._gasoline >= 150: self._gasoline = 150

car = Car('カローラ')
car.gasoline = 160 # インスタンス変数のようにあつかえる
print(car.car_model, car.gasoline)

car.car_model = 'アテンザ' # setterがないためエラーとなる
