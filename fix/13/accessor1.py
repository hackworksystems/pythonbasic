class Car:
    def __init__(self, car_model):
        # 車種：
        #   初期化のときだけ設定した後は変更しない
        #   取得可能
        self.car_model = car_model
        # ガソリン：
        #   設定（外から150まで給油）可能
        #   取得（メータで残量の確認）可能
        self.gasoline = 0

car = Car('カローラ')
car.car_model = 'アテンザ'
car.gasoline = 160
