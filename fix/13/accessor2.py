class Car:
    def __init__(self, car_model):
        self._car_model = car_model # 車種
        self._gasoline = 0          # ガソリン

    # 車種のgetter
    def get_car_model(self):
        return self._car_model

    # ガソリンのgetter
    def get_gasoline(self):
        return self._gasoline

    # ガソリンのsetter
    def set_gasoline(self, gasoline):
        self._gasoline += gasoline
        # 150を上限にする
        if self._gasoline >= 150: self._gasoline = 150

car = Car('カローラ')
car.set_gasoline(160) # 給油する

print(car.get_car_model(), car.get_gasoline())
