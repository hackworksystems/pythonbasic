# モジュールのインポート
import bmi

# モジュールの関数を利用する
tanaka_bmi = bmi.get_bmi(1.70, 60)
bmi.display_bmi(tanaka_bmi)

suzuki_bmi = bmi.get_bmi(1.75, 95)
bmi.display_bmi(suzuki_bmi)