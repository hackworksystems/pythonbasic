'''BMIモジュール'''
def get_bmi(height_m, weight_kg):
    '''身長と体重からBMIを取得'''
    # BMI ＝ 体重(kg) ÷ (身長(m) × 身長(m))
    return weight_kg / (height_m ** 2)

def display_bmi(bmi):
    '''BMIから肥満度を判定して表示'''
    if bmi < 18.5:
        print('BMIは低体重')
    elif bmi < 25:
        print('普通体重')
    elif bmi < 30:
        print('肥満度1')
    elif bmi < 35:
        print('肥満度2')
    elif bmi < 40:
        print('肥満度3')
    else:
        print('肥満度4')

if __name__ == '__main__':
    print(get_bmi(1.70, 60))