class Product:
    def __init__(self, name, price):
        self.name = name
        self.price = price

class BookProduct(Product):
    def __init__(self, name, price, publisher):
        super().__init__(name, price)
        self.publisher = publisher


    
book1 = BookProduct('Python入門', 2000, 'リナックスアカデミー')
print(book1.name, book1.price, book1.publisher)