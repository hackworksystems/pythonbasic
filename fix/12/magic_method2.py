class Car:
    def __init__(self, car_model, color):
        self.car_model = car_model
        self.color = color
    
    # 車種と色が一致した場合同じ車であるとする
    def __eq__(self, car):
        return (self.car_model == car.car_model) \
            & (self.color == car.color)

# 異なる3つのオブジェクトを生成する
car_a = Car("アテンザ", "白")
car_b = Car("アテンザ", "白")
car_c = Car("アテンザ", "青")

# 比較演算子（==）にて比較した結果を出力する
print(car_a == car_b)
print(car_a == car_c)
