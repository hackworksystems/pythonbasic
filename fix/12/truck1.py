# スーパークラス
class Car:
    '''車を表すクラス'''
    def __init__(self, car_model):
        self.car_model = car_model  # 車種
        self.direction = '前'       # 進行方向
        self.speed = 0              # スピード

    def handle(self, direction):
        '''進行方向を変更する'''
        self.direction = direction

    def accelerate(self, speed):
        '''加速する'''
        self.speed = speed

# サブクラス
class Truck(Car):
    '''トラックを表すクラス'''
    def __init__(self, car_model):
        super().__init__(car_model)
        self.cargoes = [] # 貨物

    def add_cargo(self, cargo):
        '''荷台に貨物をのせる'''
        self.cargoes.append(cargo)

truck = Truck('フォワード') # トラックのオブジェクトを生成
truck.add_cargo('テーブル') # 荷台に貨物をのせる
truck.accelerate(20)       # 加速する
print(truck.car_model, truck.speed, truck.cargoes)
