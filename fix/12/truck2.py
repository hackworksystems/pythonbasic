class Car:
    '''車を表すクラス'''
    def __init__(self, car_model):
        self.car_model = car_model  # 車種
        self.direction = '前'       # 進行方向
        self.speed = 0              # スピード

    def handle(self, direction):
        '''進行方向を変更する'''
        self.direction = direction

    def accelerate(self, speed):
        '''加速する'''
        self.speed = speed

class Truck(Car):
    '''トラックを表すクラス'''
    def __init__(self, car_model):
        super().__init__(car_model)
        self.cargoes = [] # 貨物

    def add_cargo(self, cargo):
        '''荷台に貨物をのせる'''
        self.cargoes.append(cargo)

    # オーバーライド
    def accelerate(self, speed):
        '''加速する'''
        self.speed = speed * 1.5

car = Car('カローラ')
car.accelerate(10) # 車のアクセルを踏む
truck = Truck('フォワード')
truck.accelerate(10) # トラックのアクセルを踏む
print(car.car_model, car.speed)
print(truck.car_model, truck.speed)


