class Point:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y
    def get_point_info(self):
        print('x', self.x, 'y', self.y)

class ColorPoint(Point):
    def __init__(self, color, x = 0, y = 0):
        super().__init__(x, y)
        self.color = color
    def get_point_info(self):
        print('color', self.color, 'x', self.x, 'y', self.y)

cp = ColorPoint('red', 1, 3)
cp.get_point_info()