import json
# 辞書形のユーザ情報
user = {
    'name': '鈴木一郎',
    'age': 20,
    'tel': ['0312345678', '09012345678']
}
# JSON書き込み
file_name = 'json_file.txt'
with open(file_name, 'w') as j_file:
    json.dump(user, j_file) # JSONファイルへの出力

# JSON読み込み
with open(file_name, 'r') as j_file:
    read_json = json.load(j_file)
    print(read_json)