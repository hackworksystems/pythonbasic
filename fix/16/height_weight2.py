# モジュールをインポート
import numpy as np
import matplotlib.pyplot as plt

# 身長・体重データ10人分 （[身長cm, 体重kg] × 10件）
dataset = np.array([
    [178, 76],
    [164, 60],
    [143, 50],
    [198, 89],
    [201,100],
    [177, 85],
    [165, 62],
    [156, 43],
    [155, 39],
    [165, 54],
    [177, 69],
    [189, 80],
    [173, 83],
])

# 平均の身長と体重を取得
m = np.mean(dataset, axis=0)
# 共分散行列を取得
c = np.cov(dataset, rowvar=0)
# 平均値と共分散行列に沿ったランダムなデータを100件生成
new_dataset = np.random.multivariate_normal(m, c, 100)

# 身長列の配列を取得
heights = new_dataset.T[0]
# 体重列の配列を取得
weights = new_dataset.T[1]

#  散布図の作成と表示
plt.scatter(heights, weights)
plt.title('Height & Weight')
plt.xlabel('cm')
plt.ylabel('kg')
plt.grid()
plt.show()
