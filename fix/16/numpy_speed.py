import numpy as np
import time

# 計算速度を計測する関数
def calc_speed(obj):
    start_time = time.time()
    for _ in range(1000):
        np.sum(obj)
    return (time.time() - start_time)

# ランダム値の要素を10万件持つndarrayを生成して計測
a = np.random.randn(100000)
print("ndarray\t", calc_speed(a))

# listに変換したものを計測
b = list(a)
print("list\t", calc_speed(b))
