# モジュールをインポート
import numpy as np
import matplotlib.pyplot as plt

# 身長と体重のデータをそれぞれ配列で10件ずつ用意
heights = np.array([178,164,143,198,201,177,165,156,155,165,177,189, 173])
weights = np.array([76,60,50,89,100,85,62,43,39,54,69,80,83])

# 散布図の作成
plt.scatter(heights, weights)
plt.title('Height & Weight') # タイトルを設定
plt.xlabel('cm') # X軸に単位を設定
plt.ylabel('kg') # Y軸に単位を設定
plt.grid() # グリッド線を設定
plt.show() # グラフを表示
