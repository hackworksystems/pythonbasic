#!/usr/local/bin/python3
import sys, io, html, cgi
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

name = subject = body = '' # 空文字で初期化
form = cgi.FieldStorage() # フォームデータを取得

if ('name' in form):
    name = html.escape(form.getvalue('name'))
if ('subject' in form):
    subject = html.escape(form.getvalue('subject'))
if ('body' in form):
    body =  html.escape(form.getvalue('body')).replace('\n','<br>')

print('Content-Type: text/html; charset=utf-8') # ヘッダ情報を出力
print() # 空行を出力

html = '''
<!DOCTYPE html>
<html><head>
<meta charset="utf-8">
<title>PythonでWebページ</title></head>
<body>
<h1>送信完了</h1>
<p>以下の内容で受け付けました。</p>
<dl>
<dt>お名前</dt><dd>{name}</dd>
<dt>件名</dt><dd>{subject}</dd>
<dt>本文</dt><dd>{body}</dd>
</dl>
</body></html>
'''
print(html.format(name=name, subject=subject, body=body)) # HTMLを出力
