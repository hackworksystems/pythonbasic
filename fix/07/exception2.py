score_list = [80, 90, 100]
index = input('取得する番号を入力して下さい：')
try:
    print(score_list[int(index)])
except ValueError:
    print('入力された値が整数ではありません')
except IndexError:
    print('リストの範囲外です')
