try:
    score = int(input('点数を入力して下さい：'))
    if score < 0 | score > 100:
        print('点数は0から100で入力して下さい')
    elif score >= 60:
        print('合格です')
    else:
        print('不合格です')
except:
    print('点数は数値で入力して下さい')