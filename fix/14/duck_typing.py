# トラック
class Truck:
    def run(self):
        print('トラック「ドゴゴゴゴゴゴ...」')

# 電気自動車
class EvCar:
    def run(self):
        print('電気自動車「スー...」')

# 引数で受け取ったオブジェクトのrun()メソッドを実行
def car_run(obj):
    obj.run()

car_a = Truck()
car_b = EvCar()

# 同じ関数で別のオブジェクトのメソッドを実行
car_run(car_a)
car_run(car_b)