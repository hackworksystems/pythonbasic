# ABCモジュールのインポート
from abc import ABCMeta, abstractmethod

# 抽象基底クラス
class Car(metaclass=ABCMeta):
    @abstractmethod
    def handle(self):
        pass

    @abstractmethod
    def accelerate(self):
        pass

# 抽象クラスを継承
class Truck(Car):
    def handle(self, direction):
        self.direction = direction

    def accelerate(self, speed):
        self.speed = speed

truck = Truck()
truck.handle('左')
truck.accelerate(10)
print(truck.direction, truck.speed)
