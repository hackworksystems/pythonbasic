rainy_percent = int(input('降水確率を入力して下さい：'))
if 70 <= rainy_percent:
    print('傘を必ず持って行きましょう')
elif 20 <= rainy_percent:
    print('傘はあった方がいいかもしれません')
else:
    print('傘はいらないでしょう')