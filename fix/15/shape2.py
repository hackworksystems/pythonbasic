# ABCモジュールのインポート
from abc import ABCMeta, abstractmethod

# 抽象基底クラス
class Shape(metaclass=ABCMeta):
    @abstractmethod
    def area(self):
        pass

    @staticmethod
    def check(data):
        if (1 <= data and data <= 20):
            return True
        else:
            print("入力エラー")
            exit()

# 四角形クラス
class Rectangle(Shape):
    point = 4 # クラス変数

    def __init__(self, height, width):
        self.height = height # 高さ
        self.width = width   # 幅

    @property # 高さのgetter
    def height(self):
        return self._height

    @height.setter # 高さのsetter
    def height(self, height):
        if (Shape.check(height)):
            self._height = height

    @property # 幅のgetter
    def width(self):
        return self._width

    @width.setter # 幅のsetter
    def width(self, width):
        if (Shape.check(width)):
            self._width = width

    @property # 面積のgetter
    def area(self):
        return self.height * self.width # 高さ×幅

# 三角形クラス
class Triangle(Shape):
    point = 3

    def __init__(self, base, height):
        self.base = base # 底辺
        self.height = height # 高さ

    @property # 底辺のgetter
    def base(self):
        return self._base

    @base.setter # 底辺のsetter
    def base(self, base):
        if (Shape.check(base)):
            self._base = base

    @property # 高さのgetter
    def height(self):
        return self._height

    @height.setter # 高さのsetter
    def height(self, height):
        if (Shape.check(height)):
            self._height = height

    @property # 面積のgetter
    def area(self):
        return self.base * self.height / 2


# 円クラス
class Circle(Shape):
    point = 0
    pi = 3.14

    def __init__(self, diameter):
        self.diameter = diameter

    @property # 直径のgetter
    def diameter(self):
        return self._diameter

    @diameter.setter # 直径のsetter
    def diameter(self, diameter):
        if (Shape.check(diameter)):
            self._diameter = diameter

    @property # 半径のgetter
    def radius(self):
        return self.diameter / 2

    @property # 面積のgetter
    def area(self):
        return self.radius * self.radius * Circle.pi
