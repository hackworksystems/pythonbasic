# ABCモジュールのインポート
from abc import ABCMeta, abstractmethod

# 抽象基底クラス
class Shape(metaclass=ABCMeta):
    @abstractmethod
    def area(self):
        pass

# 四角形クラス
class Rectangle(Shape):
    point = 4 # クラス変数

    def __init__(self, height, width):
        self._height = height # 高さ
        self._width = width   # 幅

    @property # 面積のgetter
    def area(self):
        return self._height * self._width # 高さ×幅

# 三角形クラス
class Triangle(Shape):
    point = 3

    def __init__(self, base, height):
        self._base = base # 底辺
        self._height = height # 高さ

    @property # 面積のgetter
    def area(self):
        return self._base * self._height / 2


# 円クラス
class Circle(Shape):
    point = 0
    pi = 3.14

    def __init__(self, diameter):
        self._diameter = diameter
        self._radius = diameter / 2

    @property # 面積のgetter
    def area(self):
        return self._radius * self._radius * Circle.pi
