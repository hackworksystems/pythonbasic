from shape2 import Rectangle, Triangle, Circle

h = int(input("長方形の高さ："))
w = int(input("長方形の幅："))
rect = Rectangle(h, w)

b = int(input("三角形の底辺："))
h = int(input("三角形の高さ："))
tri = Triangle(b, h)

d = int(input("円の直径："))
cir = Circle(d)

total_area = rect.area + tri.area + cir.area
print("面積の合計は{}平方cmです。". format(total_area))
