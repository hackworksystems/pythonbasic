# 関数定義
def add_func(func):
    return func(1, 2)
    
# add_funcの引数に無名関数を指定できる
print(add_func(lambda x, y: x + y)) 
