# 関数の定義
def add_int_function(x, y):
    return x + y

# 関数の定義
def add_str_function(x, y):
    return str(x) + str(y)

# 関数オブジェクトを引数に受け取ることを想定した関数
def functionX(func):
    return func(1, 2)

# 関数を変数に代入する
func1 = add_int_function
func2 = add_str_function
# 代入された関数を引数として使う
print(functionX(func1))
print(functionX(func2))
