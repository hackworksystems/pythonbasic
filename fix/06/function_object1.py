# 関数の定義
def add_function(x, y):
    return x + y

# 関数を変数に代入する
func = add_function
# 代入された関数を使う
print(func(1, 2))
# 関数オブジェクトの内容を出力
print(func)