# 関数の定義
def funcA(x):
    return 2 * x

# 関数の利用
print(funcA(0))
print(funcA(1))
print(funcA(2))