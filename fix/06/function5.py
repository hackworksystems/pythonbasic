# 受け取った引数をすべて足し合わせる関数
def sum(*args):
    sum_v = 0
    for n in args:
        sum_v += n
    return sum_v

print(sum(1))
print(sum(1, 2))
print(sum(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))