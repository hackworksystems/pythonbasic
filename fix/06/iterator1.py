for i in range(0, 3): # range()関数を利用した場合
    print(i)
print('---')
for i in [0, 1, 2]: # リストを利用した場合
    print(i)
print('---')
for i in (0, 1, 2): # タプルを利用した場合
    print(i)
print('---')
for key in {'a': 0, 'b': 1, 'c': 2}: # リストを利用した場合
    print(key)
print('---')
for c in 'abc': # リストを利用した場合
    print(c)