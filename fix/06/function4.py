def checkPassed(eng_score, math_score, is_recommend = False):
    if is_recommend: # 推薦
        return '合格'
    elif eng_score >= 60 and math_score >= 60: # 点数判定
        return '合格'
    else:
        return '不合格'

print(checkPassed(eng_score = 90, math_score = 55, is_recommend = True))