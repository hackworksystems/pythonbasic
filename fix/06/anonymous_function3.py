# テストの点数を辞書型で定義
score = {
    'english': 60,
    'mathematics': 100,
    'science': 50,
    'society': 82
}

# ソートする
sorted_score = sorted(
    score.items(),        # 第1引数：イテラブルオブジェクト
    key = lambda x: x[1], # 第2引数：並び替え対象項目の指定
    reverse = True        # 第3引数：True(降順) False(昇順：デフォルト)
)
print(score)
print(sorted_score)

