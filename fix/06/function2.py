# 合否判定関数
def checkPassed(eng_score, math_score, is_recommend):
    if is_recommend: # 推薦
        return '合格'
    elif eng_score >= 60 and math_score >= 60: # 点数判定
        return '合格'
    else:
        return '不合格'

print(checkPassed(90, 55, True))  # Aさん
print(checkPassed(90, 55, False)) # Bさん
print(checkPassed(90, 85, False)) # Cさん
