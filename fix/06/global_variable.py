word = 'ABC' # 関数の外で変数を宣言
def alter_word():
    global word # グローバル宣言
    word = 'DEF' # wordを書き換える

alter_word() # 関数呼び出し
print(word)