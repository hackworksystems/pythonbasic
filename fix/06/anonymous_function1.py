# 関数定義
def add_func(x, y):
    return x + y

# 無名関数
add_lam = lambda x, y: x + y

print(add_func(1, 2))
print(add_lam(1, 2))
