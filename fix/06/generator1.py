# ジェネレータの定義
def genWord():
    yield 'ABC'
    yield 'DEF'
    yield 'GHI'

# for構文内でイテレータとして利用する
for str in genWord():
    print(str)