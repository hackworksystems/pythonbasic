def checkPassed(eng_score, math_score, is_recommend):
    '''
    合否判定関数
    Parameters
    ----------
    eng_score : int
        英語の点数
    math_score : int
        数学の点数
    is_recommend : bool
        推薦可否
    
    Returns
    -------
    '合格' : str
        推薦の場合、または、英数の点数が60点以上
    '不合格' : str
        上記以外
    '''
    if is_recommend: # 推薦
        return '合格'
    elif eng_score >= 60 and math_score >= 60: # 点数判定
        return '合格'
    else:
        return '不合格'