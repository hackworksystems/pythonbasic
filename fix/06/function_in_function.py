def function_out(a, b):
    # 関数内関数
    def function_in(x, y):
        return x + y

    return '{0}+{1}={2}'.format(a, b, function_in(a, b))

print(function_out(1, 2))
