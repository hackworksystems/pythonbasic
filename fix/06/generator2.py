# startからendまでの偶数を2倍して返す
def gen_func2(start, end):
    for i in range(start, end + 1):
        if i % 2 == 0:
            yield i * 2

for i in gen_func2(4, 10):
    print(i)
