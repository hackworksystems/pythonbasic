def is_leap_year(year):
    if year % 400 == 0:
        return True
    elif (year % 100 != 0) & (year % 4 == 0):
        return True
    else:
        return False

input_year = int(input('年を入力して下さい：'))
if is_leap_year(input_year):
    print('うるう年です')
else:
    print('うるう年ではありません')
